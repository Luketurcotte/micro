- Definition:
	- Teaching method named after one of the great Greek philosophers "Socrates"
		- Socrates believed that his greatest skill was his ability to help others do their own thinking in a way that lead to the creation of new ideas
	- Socratic method is a shared dialogue between teacher and student; wherein the teacher leads by posing thought provoking questions 
	that are intended to simulate critial thinking, draw ideas, and expose underlying presumptions 
		- If we were to think about blooom's taxonomy, the Socratic method is promote analysis and evaluation, which are two higher order skills
	- In contrast to a lot of teaching methods, one could argue that the Socratic method isn't actually teaching
		- The teacher is neither the "sage on the stage" nor the "guide on the side" 
		- The students are not passive receipients of knowledge; they have a very active role in the learnign process 
	- I will play a short video clip in a moment that illustrates the Socratic method, one thing that I hope that you can appreciate though is that the Socratic method relies on something called "Productive Discomfort" 
		- Practicioners of the Socratic method believe that this discomfort illicited in the classroom is necessary for enabling critical thinking when discussing ideas of complexity, difficulity and uncertainty

- Learning Objectives:
	- Define the Socratic teaching method
	- Identify strategies for delivering an effective lesson using the Socratic method

- PLAY THE VIDEO - Video is from the movie "The Paper Chase"
	- There are numerous opponents to the Socratic teaching method; as we saw in the video the Socratic method is somewhat adversarial
	- Here's a quote from an opinion piece published online, "The Socratic methods imparts an aggressive classroom tone that subjects students to the panic that suddenly being put on the spot can invoke, along with the fear of knowing a cold call is imminent. This can prompt a flight or fight reaction, causing the student to shut down, freeze, dissociate, and/or experience a flashback or panic attack." 

- Discussion
	- "Do you think it's wrong to put students on the spot and use the Socratic method?"
	- "Some people say that the Socratic method should only have a place in classrooms where students will be practitioners in fields where this type of "on the spot" thinking is necessary, do you think that the Socratic method has a place in conventional classrooms?" 
	- "How can the Socratic method be integrated effectively into classroom settings today?"
		- Bloom's Taxonomy (Discussion promotes analysis and evaluation, two high level skills) 
		- 

- Points for Board
	- Set conversational guidelines
		- Learn student names
		- Explain that participation requires listening and active engagement and that it is not enough to just insert a single comment in class and then be silent for the rest of the day
	- Smaller class sizes 
	- Focus on concepts and principles - not first person narratives
	- Be comfortable with the silence - be willing to wait 10 seconds
	- Provide students with the opportunity to think before being asked to respond
	- Be willing to follow-up, don't just accept any answer
	- Need to be flexible (not knowing how the lesson will turn out)
	- Some degree of background reading can be useful

- Learning Objectives:
	- Define the Socratic teaching method
	- Identify strategies for delivering an effective lesson using the Socratic method
- Final Question:
	- "Why or why not should the Socratic method be used in Today's classrooms?"
